module.exports = {
	// dev server配置
	host: '0.0.0.0',
	port: 3000,
	// 自动打开浏览器
	open: false,
	// start 环境变量
	devEnv: {
		NODE_ENV: '"development"',
		// 用于在开发环境进行预览和静态页测试
		PRE_ENV: true,
		STATIC_ENV: false
	}
}
