var webpack = require('webpack')
const merge = require('webpack-merge')
const baseWebpackConfig = require('./webpack.base.conf')
const tinypngKey = require('../tinypng-key.json')
const TinypngWebpackPlugin = require('tinypng-webpack-plugin')
const webpackConfig = merge(baseWebpackConfig, {
	output: {
		filename: 'js/[name].bundle.[chunkhash].js'
	},
	plugins: [
		new webpack.DefinePlugin({
			'process.env': {
				NODE_ENV: '"production"',
				// 用于打包	静态页和预览页
				STATIC_ENV: process.env.STATIC_ENV
			}
		}),
		new TinypngWebpackPlugin({
			ext: ['png', 'jpeg', 'jpg'],
			key: tinypngKey
		}),
		new webpack.optimize.UglifyJsPlugin({
			uglifyOptions: {
				compress: {
					warnings: false
				}
			},
			parallel: true,
			sourceMap: true
		})
	],
	devtool: 'hidden-module-source-map'
})

module.exports = webpackConfig
