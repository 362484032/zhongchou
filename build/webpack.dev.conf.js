var webpack = require('webpack')
const FriendlyErrorsPlugin = require('friendly-errors-webpack-plugin')
const merge = require('webpack-merge')
var path = require('path')
const baseWebpackConfig = require('./webpack.base.conf')
const portfinder = require('portfinder')
var ExtractTextPlugin = require('extract-text-webpack-plugin')

const config = require('./config')
const HOST = config.host || '0.0.0.0'
const PORT = config.port || 3000
const OPEN = config.open || false
const devWebpackConfig = merge(baseWebpackConfig, {
	plugins: [
		new webpack.DefinePlugin({
			'process.env': config.devEnv
		}),
		new webpack.HotModuleReplacementPlugin(),
		new ExtractTextPlugin({
			filename: 'css/[name].bundle.[chunkhash].css',
			disable: true
		})
	],
	devServer: {
		contentBase: path.join(__dirname, 'public'),
		disableHostCheck: true,
		host: HOST,
		port: PORT,
		compress: true,
		hot: true,
		open: OPEN
	}
})

module.exports = new Promise((resolve, reject) => {
	portfinder.basePort = PORT
	portfinder.getPort((err, port) => {
		if (err) {
			reject(err)
		} else {
			// publish the new Port, necessary for e2e tests
			process.env.PORT = port
			// add port to devServer config
			devWebpackConfig.devServer.port = port

			// Add FriendlyErrorsPlugin
			devWebpackConfig.plugins.push(
				new FriendlyErrorsPlugin({
					compilationSuccessInfo: {
						messages: [
							`Your application is running here: http://${
								devWebpackConfig.devServer.host
							}:${port}`
						]
					},
					onErrors: err => {
						console.log(err)
					}
				})
			)
			resolve(devWebpackConfig)
		}
	})
})
