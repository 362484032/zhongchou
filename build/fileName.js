const pkg = require('../package.json')
const productionName = pkg.name

const STATIC_ENV = process.env.STATIC_ENV
const PRE_ENV = process.env.PRE_ENV

let name = productionName

if (STATIC_ENV) {
	name += '-static'
} else if (PRE_ENV) {
	name += '-preview'
}

module.exports = name
