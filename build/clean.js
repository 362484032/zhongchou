const rimraf = require('rimraf')
var productionName = require('./fileName')

const distZipPath = './dist/' + productionName + '.zip'
const distPath = './dist/' + productionName

function rmByPath(path) {
	if (typeof path !== 'string') return
	rimraf(path, function(err) {
		if (err) console.log(err)
	})
}

rmByPath(distPath)

rmByPath(distZipPath)
