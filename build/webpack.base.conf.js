var webpack = require('webpack')
var HtmlWebpackPlugin = require('html-webpack-plugin')
var ExtractTextPlugin = require('extract-text-webpack-plugin')
var path = require('path')
const os = require('os')
var HappyPack = require('happypack')
var happyThreadPool = HappyPack.ThreadPool({ size: os.cpus().length })
var productionPath = require('./fileName')

// TODO clean plugin instead of rm -rf
function resolve(dir) {
	return path.join(__dirname, '..', dir)
}

module.exports = {
	// ordered path
	context: path.resolve(__dirname, '../'),
	entry: {
		index: ['whatwg-fetch', './src/index.jsx']
	},

	output: {
		path: resolve('/dist/' + productionPath),
		filename: 'js/[name].bundle.js'
	},

	module: {
		rules: [
			{
				test: /\.jsx?$/,
				exclude: /node_modules/,
				loader: 'happypack/loader?id=react'
			},
			{
				test: /\.(scss|css)/,
				loader: ExtractTextPlugin.extract({
					fallback: 'style-loader',
					use: [
						{
							loader: 'css-loader',
							options: {
								minimize: true,
								// set true will let postcss dosen't work
								autoprefixer: false
							}
						},
						{
							loader: 'postcss-loader',
							// postcss loader setting
							options: {
								ident: 'postcss',
								plugins: loader => [
									require('autoprefixer')({
										browsers: [
											'Android >= 4.0',
											'iOS >= 6',
											'last 5 QQAndroid versions',
											'last 5 UCAndroid versions'
										],
										cascade: true
									}),
									require('postcss-px2rem')({
										remUnit: 75
									})
								]
							}
						},
						'sass-loader'
					],
					publicPath: '../'
				})
			},
			{
				test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
				loader: 'url-loader',
				options: {
					limit: 4000,
					name: 'img/[name].[hash:7].[ext]'
				}
			},
			{
				test: /\.json/,
				loader: 'json-loader'
			}
		]
	},

	plugins: [
		new webpack.ProvidePlugin({
			Promise: 'promise-polyfill'
		}),
		new HtmlWebpackPlugin({
			filename: './index.html', //相对publicPath
			template: './src/index.html', //相对config
			inject: 'body',
			hash: false
		}),
		new ExtractTextPlugin('css/[name].bundle.[chunkhash].css'),
		new HappyPack({
			id: 'react',
			threadPool: happyThreadPool,
			loaders: [
				{
					loader: 'babel-loader',
					options: {
						// set cache
						cacheDirectory: '.webpack_cache'
					}
				}
			]
		})
	],

	resolve: {
		modules: [resolve('src'), resolve('node_modules')],
		extensions: ['.js', '.jsx'],
		alias: {
			'@': path.resolve('src')
		}
	},
	devtool: 'source-map'
}
