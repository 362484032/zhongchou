import './monitors'
import './css/main.scss'

import Anthem from './modules/anthem.core'
import * as modules from './components/pages/exportModules'
import App from './components/pages/App'
// import { isProduction } from './utils/utils'

// if (!isProduction()) {
// 	const Vconsole = require('vconsole')
// 	new Vconsole()
// }

// ReactDOM.render(<App />, document.getElementById('app'));

const app = new Anthem()
// 先挂载modules
app.addExports(modules)
// 再启动
app.start(App, document.getElementById('app'))

// export store 使得可以在任何地方使用 dispatch
// 例如：沉浸式导航需要在 window 的回调函数中 dispatch action
export const { store } = app
