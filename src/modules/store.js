/*
 * @Author: wangbing198
 * @Date: 2020-10-21 10:33:01
 * @LastEditTime: 2020-10-29 17:28:35
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: /crowd-funding/src/modules/store.js
 */
import { applyMiddleware, compose, createStore } from 'redux'
import createSagaMiddleware from 'redux-saga'

export default (initState = {}, rootReducer, rootSaga) => {
	const sagaMiddleware = createSagaMiddleware()
	let store

	if (process.env.NODE_ENV === 'production') {
		store = createStore(
			rootReducer,
			initState,
			compose(applyMiddleware(sagaMiddleware))
		)
	} else {
		const composeEnhancers =
			window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
		store = createStore(
			rootReducer,
			initState,
			composeEnhancers(applyMiddleware(sagaMiddleware))
		)
	}
	sagaMiddleware.run(rootSaga)

	return store
}
