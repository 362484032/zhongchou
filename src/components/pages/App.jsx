/*
 * @Author: your name
 * @Date: 2020-10-21 10:33:01
 * @LastEditTime: 2020-10-21 11:09:13
 * @LastEditors: your name
 * @Description: In User Settings Edit
 * @FilePath: /crowd-funding/src/components/pages/App.jsx
 */
import React, { Component } from 'react'
import Homepage from '../homePage/index'

export default class App extends Component {
	render() {
		return (
			<div>
				<Homepage />
			</div>
		)
	}
}
