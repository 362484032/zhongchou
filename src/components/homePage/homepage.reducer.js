export const storyInfoReducer = function(
	state = { isLoading: false },
	{ type, payload } = {}
) {
	switch (type) {
		case 'set:storyInfo': {
			return { ...state, ...payload }
		}
		default:
			return state
	}
}
