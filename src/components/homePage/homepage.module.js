import { storyInfoReducer } from './homepage.reducer'
import { storyPageInfo } from './mockData'
import { all, fork, put } from 'redux-saga/effects'

function* init() {
	console.log('22222')
	yield put({
		type: 'set:storyInfo',
		payload: storyPageInfo
	})
}

export default {
	reducer: {
		storyInfoReducer
	},
	*saga() {
		yield all([fork(init)])
	}
}
