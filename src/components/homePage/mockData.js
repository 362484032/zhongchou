export const storyPageInfo = {
	code: 0,
	data: {
		biz_code: 1000,
		biz_msg: '解析项目信息成功',
		result: {
			projectRedoundListDto: [
				{
					amount: 279, //金额
					supportLimit: 3, //支持数量限额
					canBuy: true, //是否可以购买
					giftFlag: 1, //是否含赠品
					limits: 3000, //回报上线
					supportNum: 10, //已支持数量 用于计算库存 limits - supportNum为该档位剩余份数
					mAmount: 0, //M金额
					platformType: 0, //平台类型 默认0（普通）1(M专享) 2（M优惠）
					projectId: 1854507660, //项目ID
					redoundContent:
						'感谢您的支持，支持者将获得京东京造智能颈椎按摩器Air 绿*1   发货时间：众筹结束后，10.26日开始陆续发货。', //回报内容
					redoundDays: 5, //回报天数
					redoundId: 524135, //档位ID
					freight: 8.0, //运费
					redoundSpecList: [
						//档位规格
						{
							id: 245861,
							projectId: 1854507660, //项目ID
							redoundId: 524135, //档位id
							specDetail: '绿', //规格详情
							specList: ['绿'],
							specName: '颜色'
						}
					],
					redoundThumImage:
						'//img30.360buyimg.com/cf/s128x128_jfs/t1/140093/39/10959/33652/5f87c60cEc4b5a097/f461e6e8ca224b73.jpg' //回报缩略图片
				},
				{
					amount: 279,
					supportLimit: 3,
					canBuy: true,
					giftFlag: 1,
					limits: 2000,
					supportNum: 9,
					mAmount: 0,
					platformType: 0,
					projectId: 1854507660,
					redoundContent:
						'感谢您的支持，支持者将获得京东京造智能颈椎按摩器Air 玫瑰金*1 发货时间：众筹结束后，10.26日开始陆续发货。',
					redoundDays: 5,
					redoundId: 524136,
					freight: 8.0, //运费
					redoundSpecList: [
						{
							id: 245994,
							projectId: 1856520003,
							redoundId: 524379,
							specDetail: '黑色',
							specList: ['黑色'],
							specName: '颜色'
						},
						{
							id: 245995,
							projectId: 1856520003,
							redoundId: 524379,
							specDetail: '35|36|37|38|39|40|41|42|43|44',
							specList: [
								'35',
								'36',
								'37',
								'38',
								'39',
								'40',
								'41',
								'42',
								'43',
								'44'
							],
							specName: '尺寸'
						}
					],
					redoundThumImage:
						'//img30.360buyimg.com/cf/s128x128_jfs/t1/130314/31/12047/15309/5f812a6dE9b70292b/ad1c4995f4c15fc8.jpg'
				}
			],
			projectStoryNewDto: {
				pre: '2020-09-15 23:42:45', //项目开始时间
				endTime: '2020-10-25 08:58:00', //项目结束时间
				systemTime: '2020-09-14 23:42:45', //服务器时间
				buttonStatus: 2, //1 去预约 ,2立即支持 ,3众筹成功结束 ,4众筹失败结束 ,5购买现货
				amount: 50000, //募集金额目标
				appShowImg:
					'//img30.360buyimg.com/cf/jfs/t1/122787/32/14146/185636/5f87b87aE203d0202/f9695fbb44ab4531.jpg', //项目竖图（故事页面使用）
				collectedAmount: 52731, //已募集金额
				devoteCount: 0, //已打赏者数量
				minRedoundAmount: 279, //超值档位金额
				productId: 31234144, //产品Id 点击"购买现货" 跳转https://cz-m.jd.com/product/details/${productId}.htm
				projectStatus: 5, //项目状态 projectStatus :4 去提醒 projectStatus :6 去支持 projectStatus :7，9，11，12，13，14未成功 projectStatus :8 众筹成功，如果有productId，购买现货projectStatus :10时，如果closureType为1且如果有productId，购买现货，没有的话众筹成功 closureType不为1则展示众筹未成功
				followAmount: 20300, //项目关注人数
				closureType: 1, //项目结束标识
				preheatEndDays: 1, //预热剩余天数
				preheatEndHours: 10, //预热剩余小时
				remainDays: 3, //项目众筹剩余时间(天)
				remainhours: 3, //项目众筹剩余时间（小时）
				officialPhone: '400-603-8218', //官方电话
				padvantageList: [
					//项目亮点
					{
						advantege: '低头族神器', //亮点
						advantegeAdr: '',
						createdDate: '2020-10-10 10:35:49', //创建时间
						id: 69904, //主键
						modifiedDate: '2020-10-10 10:35:49', //修改时间
						projectId: 1854507660, //项目ID
						yn: 1 //逻辑删除标志,1有效，2删除
					},
					{
						advantege: '时尚舒适',
						advantegeAdr: '',
						createdDate: '2020-10-10 10:35:49',
						id: 69905,
						modifiedDate: '2020-10-10 10:35:49',
						projectId: 1854507660,
						yn: 1
					}
				],
				progress: '105', //筹集进度
				projectAdWord:
					'微电流仿真按摩 20档强度 2档热敷 2H续航 语音播报 仅100g', //宣传语
				projectId: 1854507660, //项目ID
				projectName: '京东京造智能颈椎按摩器Air', //项目名称
				projectThumImage1:
					'//img30.360buyimg.com/cf/jfs/t1/119406/14/18754/136095/5f87b864E806898b3/7a8bfb285fbfd727.jpg', //1080*1080 图片
				projectType: 8, //项目类型
				shipType: 0, //是否已经开始发货
				supports: 189, //已支持人数
				workTime: '周一至周日 09:00-20:00' //办公时间
			}
		},
		success: true
	},
	rtn_code: 0
}
