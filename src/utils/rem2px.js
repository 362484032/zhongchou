export default function rem2px(rem) {
	let px = (rem * window.screen.width) / 375
	px = px.toFixed(2)
	return `${px}px`
}
