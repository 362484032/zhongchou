/*
 * @Author: zhanghong
 * @Date: 2019-01-17 17:27:10
 * @Description: 埋点上报函数
 */
export function reportPV() {
	try {
		let pv = new MPing.inputs.PV()
		let mping = new MPing()
		mping.send(pv)
	} catch (e) {}
}

export function reportClick(opts) {
	return new Promise((resolve, reject) => {
		try {
			let click = new MPing.inputs.Click(opts.eventId)

			if (opts.eventParam) click.event_param = opts.eventParam
			if (opts.eventLevel) click.event_level = opts.eventLevel

			click.updateEventSeries()

			let mping = new MPing()
			mping.send(click)
			setTimeout(() => resolve(), 300)
		} catch (e) {
			reject(e)
		}
	})
}
