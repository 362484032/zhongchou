import jdShare from '@jmfe/jm-jdshare'
import { url, fromName } from './constants'
import { isApp } from '@jmfe/jm-common'
import { formatImageUrl } from './imgFormatter'
// 设置分享
export function setShare(shareParam) {
	let shareInfo = {
		url: `${url}?fromName=${fromName}&ad_od=1` || window.location.href,
		img: (() => {
			if (shareParam.wxShareImg) {
				return 'https:' + formatImageUrl(shareParam.wxShareImg, '100x100')
			}
			//return 'https://img11.360buyimg.com/jdphoto/s80x80_jfs/t27847/91/107794072/6854/14716732/5b850ecaN644d2983.png'
			return 'https://m.360buyimg.com/babel/jfs/t1/136996/8/1885/3985/5ee0b735E683dca0f/6f5242605440615c.jpg'
		})(),
		title: shareParam.wxShareTitle || '京东金榜让购物更省心',
		content:
			shareParam.wxShareSubTitle ||
			'购物担心入错坑？金榜权威数据为你提供购物新指南！',
		desc:
			shareParam.wxShareSubTitle ||
			'购物担心入错坑？金榜权威数据为你提供购物新指南！',
		// channel: 'Wxfriends,Wxmoments,QQfriends,QQzone,Sinaweibo,CopyURL',
		channel: '',
		timeline_title:
			shareParam.wxShareSubTitle ||
			'购物担心入错坑？金榜权威数据为你提供购物新指南！'
	}
	if (shareParam.isJWordShare == 1) {
		const keyparam = {
			url: url || window.location.href,
			keyEndTime: '1909065599000', //new Date('2030/06/30 23:59:59').getTime()
			keyChannel: 'Wxfriends,Wxmoments',
			sourceCode: 'babel',
			keyImg: shareParam.jWordShareImg, //弹窗渲染图
			keyId: '2kuDDsNmnkdsxbv8sLoZhHWtqh5Z', //活动标识传参规则,详情查看京口令使用规范&调用方式
			keyTitle: shareParam.jWordShareTitle || '京东金榜让购物更省心', //弹窗文案
			keyContent:
				shareParam.jWordShareContent ||
				'购物担心入错坑？金榜权威数据为你提供购物新指南！' //分享内容
		}
		shareInfo.keyparam = keyparam
	}
	console.log('-shareConf-', shareInfo)
	if (isApp('jd')) {
		jdShare.setShareInfo(shareInfo)
	}
}
