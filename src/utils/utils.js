export function pxTorem(px) {
	let rem = 37.5
	return `${px / rem / 2}rem`
}
export function formatComment(count) {
	if (!count) return
	try {
		switch (true) {
			case count > 0 && count < 100:
				return ''
			case count >= 100 && count < 1000:
				return count
			case count >= 1000 && count < 10000:
				return '999+'
			case count >= 10000:
				let num = (count / 10000).toString()
				let before = num.split('.')[0]
				let after = num.split('.')[1] ? num.split('.')[1].slice(0, 1) : 0
				return before + '.' + after + '万+'
			default:
				return ''
		}
	} catch (error) {
		return ''
	}
}
export function concatUrl(url, param) {
	const keys = Object.keys(param)
	const values = Object.values(param)
	let returnUrl = url + '?'
	keys.map((item, idx) => {
		returnUrl =
			returnUrl +
			item +
			'=' +
			values[idx] +
			(idx !== keys.length - 1 ? '&' : '')
	})
	return returnUrl
}
