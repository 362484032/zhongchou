var _typeof =
	'function' == typeof Symbol && 'symbol' == typeof Symbol.iterator
		? function(e) {
				return typeof e
		  }
		: function(e) {
				return e &&
					'function' == typeof Symbol &&
					e.constructor === Symbol &&
					e !== Symbol.prototype
					? 'symbol'
					: typeof e
		  }
!(function(e) {
	'undefined' != typeof module &&
	'object' === ('undefined' == typeof exports ? 'undefined' : _typeof(exports))
		? (module.exports = e)
		: 'function' == typeof define && (define.amd || define.cmd)
		? define(e)
		: (window.JD = { wxappext: e() })
})(function(e) {
	function n(e) {
		for (
			var n = ~e.indexOf('?') ? '&' : '?',
				t = [
					'PPRD_P',
					'visitkey',
					'__wga',
					'__jdv',
					'__jda',
					'unpl',
					'wq_addr',
					'wxapp_type'
				],
				o = {},
				i = 0,
				a = t.length;
			i < a;
			++i
		) {
			var r = t[i]
			if ('PPRD_P' == r) {
				for (
					var c = [],
						s = ['EA', 'CT', 'UUID', 'LOGID', 'GROUP', 'WDSTAG'],
						d = w(r) || '',
						p = 0,
						m = s.length;
					p < m;
					++p
				) {
					var l = s[p],
						f = new RegExp('(^|[~\\W])' + l + '\\.([^-]*)', 'i'),
						g = d.match(f)
					g && g.length > 1 && c.push(l + '.' + g[2])
				}
				o[r] = c.join('-')
			} else o[r] = w(r) || ''
		}
		var h = u('pps') || ''
		return (
			e +
			n +
			'cookie=' +
			encodeURIComponent(JSON.stringify(o)) +
			'&wdref=' +
			encodeURIComponent(location.href) +
			(h ? '&pps=' + h : '')
		)
	}
	function t(e, n) {
		function t() {
			o &&
				((o.onload = o.onreadystatechange = o.onerror = null),
				o.parentNode && o.parentNode.removeChild(o),
				(o = null))
		}
		var o = document.createElement('script')
		;(n = n || {}),
			(o.charset = n.charset || 'utf-8'),
			(o.onload = o.onreadystatechange = function() {
				;(/loaded|complete/i.test(this.readyState) ||
					-1 == navigator.userAgent.toLowerCase().indexOf('msie')) &&
					(n.onLoad && n.onLoad(), t())
			}),
			(o.onerror = function() {
				n.onError && n.onError(), t()
			}),
			(o.src = e),
			n.defer && (o.defer = 'defer'),
			n.async && (o.async = 'async'),
			n.crossorigin && o.setAttribute('crossorigin', 'true'),
			document.getElementsByTagName('head')[0].appendChild(o)
	}
	function o(e, n) {
		try {
			var t = JSON.parse(localStorage.getItem(h)) || {}
			;(t[e] = n), localStorage.setItem(h, JSON.stringify(t))
		} catch (e) {}
	}
	function i(e) {
		var n
		try {
			return window.JSON && (n = JSON.parse(localStorage.getItem(h)) || {})
				? n[e]
				: null
		} catch (e) {
			return null
		}
	}
	function a(e) {
		y ? (window.wx && _ ? e && e(0) : v.push(e)) : e && e(3)
	}
	function r(e) {
		for (var n = 0, t = v.length; n < t; ++n) {
			var o = v[n]
			o && o.call(null, e)
		}
		v = []
	}
	function c(n) {
		function o() {
			if (window.wx) {
				var e = [
					'checkJsApi',
					'getNetworkType',
					'onMenuShareTimeline',
					'onMenuShareAppMessage',
					'onMenuShareQQ',
					'onMenuShareWeibo',
					'hideMenuItems',
					'showMenuItems',
					'editAddress',
					'previewImage',
					'uploadImage',
					'downloadImage',
					'chooseImage',
					'setCloseWindowConfirmDialogInfo',
					'setPageTitle',
					'startRecord',
					'stopRecord',
					'onVoiceRecordEnd',
					'playVoice',
					'pauseVoice',
					'stopVoice',
					'onVoicePlayEnd',
					'uploadVoice',
					'getLocation',
					'openLocation',
					'scanQRCode',
					'launchMiniProgram'
				]
				window.wx.error(function(e) {
					;(S = !0), r(2)
				}),
					window.wx.ready(function() {
						S ||
							window.wx.checkJsApi({
								jsApiList: e,
								success: function(e) {
									;(window.wx.apiCheckResult = e.checkResult), (_ = !0), r(0)
								}
							})
					}),
					window.wx.config({
						beta: !0,
						debug: !1,
						appId: n.appId,
						timestamp: n.timestamp,
						nonceStr: n.nonceStr,
						signature: n.signature,
						jsApiList: e
					})
			}
		}
		e
			? window.seajs || 'redact.m.jd.com' == location.hostname
				? ((window.wx = e('//res.wx.qq.com/open/js/jweixin-1.3.1.js')), o())
				: e(['//res.wx.qq.com/open/js/jweixin-1.3.1.js'], function(e) {
						;(window.wx = e), o()
				  })
			: t('//res.wx.qq.com/open/js/jweixin-1.3.1.js', {
					onLoad: function() {
						o()
					},
					onError: function() {
						r(1)
					},
					defer: !0,
					async: !0
			  })
	}
	function s() {
		t(
			x +
				'?url=' +
				encodeURIComponent(location.href.split('#')[0]) +
				'&callback=getwxjssign2&t=' +
				Math.random(),
			{ defer: !0, async: !0 }
		),
			(window.getwxjssign2 = function(e) {
				0 == e.errCode && (c(e), o(g, e))
			})
	}
	function d(e) {
		var n = e || location.hash
		return n ? n.replace(/.*#/, '') : ''
	}
	function p(e) {
		var n = d().match(new RegExp('(^|&)' + e + '=([^&]*)(&|$)'), 'i')
		return null != n ? n[2] : ''
	}
	function u(e, n) {
		var t = arguments[1] || window.location.search,
			o = new RegExp('(^|&)' + e + '=([^&]*)(&|$)', 'i'),
			i = t.substr(t.indexOf('?') + 1).match(o)
		return null != i ? i[2] : ''
	}
	function w(e) {
		var n = new RegExp('(^| )' + e + '(?:=([^;]*))?(;|$)'),
			t = document.cookie.match(n)
		if (!t || !t[2]) return ''
		var o = t[2]
		try {
			return /(%[0-9A-F]{2}){2,}/.test(o) ? decodeURIComponent(o) : unescape(o)
		} catch (e) {
			return unescape(o)
		}
	}
	function m(e, n, t, o, i, a) {
		var r = new Date(),
			t = arguments[2] || null,
			o = arguments[3] || '/',
			i = arguments[4] || null,
			a = arguments[5] || !1
		t && r.setMinutes(r.getMinutes() + parseInt(t)),
			(document.cookie =
				e +
				'=' +
				escape(n) +
				(t ? ';expires=' + r.toGMTString() : '') +
				(o ? ';path=' + o : '') +
				(i ? ';domain=' + i : '') +
				(a ? ';secure' : ''))
	}
	function l(e, n, t) {
		var o = new RegExp('(^|[~\\W])' + e + '\\.([^-]*)', 'i'),
			i = n.match(o),
			a = t.match(o)
		return (
			a && a.length > 0
				? (t =
						i && i.length > 1
							? t.replace(o, '$1' + e + '.' + i[2])
							: t.replace(o, ''))
				: i && i.length > 1 && (t += (t ? '-' : '') + e + '.' + i[2]),
			t
		)
	}
	function f() {
		var e = {
				PPRD_P: 4320,
				visitkey: 518400,
				__wga: 259200,
				__jdv: 1440,
				__jda: 1440,
				unpl: 1440,
				wq_addr: 999999,
				wxapp_type: 0,
				appkey: 0,
				customerinfo: 0,
				source: 0
			},
			n = p('cookie')
		if (n && window.JSON) {
			var t = window.JSON.parse(decodeURIComponent(n))
			for (var o in t) {
				var i = t[o]
				switch (o) {
					case 'PPRD_P':
						var a = w('PPRD_P') || ''
						m(
							o,
							(a = l(
								'WDSTAG',
								i,
								(a = l(
									'GROUP',
									i,
									(a = l(
										'LOGID',
										i,
										(a = l('UUID', i, (a = l('CT', i, (a = l('EA', i, a))))))
									))
								))
							)),
							0 === e[o] ? null : e[o] || 1440,
							'/',
							'jd.com'
						)
						break
					default:
						m(o, i, 0 === e[o] ? null : e[o] || 1440, '/', 'jd.com')
				}
			}
		}
		var r = p('wdref')
		if (r)
			try {
				window.sessionStorage &&
					window.sessionStorage.setItem('refer', decodeURIComponent(r))
			} catch (e) {}
		var c = d()
		if (c) {
			var s = location.href.split('#')[0]
			location.replace(
				s + '#' + c.replace(/\&?(cookie|wdref|wxa_level)=[^&]*/gi, '')
			)
		}
	}
	var g = '_wx_signature' + encodeURIComponent(location.href.split('#')[0]),
		h = '_wx_api_store',
		x = '//wq.jd.com/bases/jssdk/GetWxJsApiSign',
		y = navigator.userAgent.toLowerCase().indexOf('micromessenger') > -1,
		v = [],
		_ = !1,
		S = !1,
		j = {
			index: '/pages/index/index',
			detail: '/pages/item/detail/detail',
			my: '/pages/my/index/index',
			cart: '/pages/cart/cart/cart',
			search: '/pages/search/list/list',
			buy: '/pages/pay/index/index',
			account: '/pages/my/account/account',
			pgitem: '/pages/item/pingou/pingou',
			pgdetail: '/pages/pingou/detail/index',
			shop: '/pages/store/index/index',
			gwqpage: '/pages/gwq/index',
			category: '/pages/cate/cate'
		},
		I = /(?:https?:)?\/\/(?:(?:wq(?:m?item)?\.jd\.(?:com|hk)\/m?item\/view)|(?:wqs\.jd\.com\/item\/jd\.shtml)).*[?&]sku=(\d+)/i,
		R = /(?:https?:)?\/\/(?:(?:item\.jd\.com\/|(?:item\.m\.jd\.com\/product\/)))(\d+)\.html/i,
		P = u('miniprogram') || '',
		k = p('wxa_level') || '',
		C = p('cookie') || '',
		b = w('wxapp_type') || 0,
		O = k
	if (
		(!O && (O = w('wxa_level') || ''),
		(O = O ? parseInt(O, 10) : 1),
		m('wxa_level', O, 1440, '/', 'jd.com'),
		b &&
			(document.referrer ||
				(!(function(e, n, t, o) {
					if (null != w(e)) {
						var i = new Date()
						i.setMinutes(i.getMinutes() - 1e3),
							(n = n || '/'),
							(document.cookie =
								e +
								'=;expires=' +
								i.toGMTString() +
								(n ? ';path=' + n : '') +
								(t ? ';domain=' + t : '') +
								(o ? ';secure' : ''))
					}
				})('wxapp_type'),
				(b = 0))),
		C && window.JSON)
	) {
		var M = {}
		try {
			M = window.JSON.parse(decodeURIComponent(C))
		} catch (e) {}
		M.wxapp_type && (b = M.wxapp_type)
	}
	var U = {
		wxappType: b || 0,
		urls: j,
		transform: {
			detail: function(e) {
				var n = e.match(I)
				if ((!n && (n = e.match(R)), n)) {
					var t = /ispg=1/.test(e),
						o = [],
						i = '',
						a = '',
						r = e.match(/name=([^&#]*)/)
					r && r.length > 1 && r[1] && (i = r[1])
					var c = e.match(/cover=([^&#]*)/)
					if (
						(c && c.length > 1 && c[1] && (a = c[1]),
						i && o.push('name=' + i),
						a)
					) {
						var s = decodeURIComponent(a)
						;/^(https?:)?\/\//.test(s)
							? ((s = s.replace(/^http:/, 'https:')),
							  /^\/\//.test(s) && (s = 'https:' + s),
							  (a = encodeURIComponent(s)))
							: ((s =
									'https://img1' +
									parseInt(5 * Math.random(), 10) +
									'.360buyimg.com/mobilecms/s350x350_' +
									s),
							  (a = encodeURIComponent(s))),
							o.push('cover=' + a)
					}
					return (
						(t ? j.pgitem : j.detail) +
						'?sku=' +
						n[1] +
						(o.length > 0 ? '&' + o.join('&') : '')
					)
				}
				return ''
			}
		},
		isWxapp: function(e) {
			P
				? e && e('1' == P)
				: b
				? e && e(!0)
				: k
				? e && e(!0)
				: a(function(n) {
						e && e(!n && 'miniprogram' == window.__wxjs_environment)
				  })
		},
		goto: function(e, t) {
			a(function(o) {
				var i =
					~e.indexOf(j.index) || ~e.indexOf(j.my) || ~e.indexOf(j.gwqpage)
						? 'switchTab'
						: 'navigateTo'
				'switchTab' != i && (O >= 8 ? (i = 'redirectTo') : t && (i = t)),
					window.wx && window.wx.miniProgram
						? window.wx &&
						  window.wx.miniProgram &&
						  window.wx.miniProgram[i]({ url: n(e) })
						: window.WeixinJSBridge &&
						  window.WeixinJSBridge &&
						  window.WeixinJSBridge.invoke('invokeMiniProgramAPI', {
								name: i,
								arg: { url: n(e) }
						  })
			})
		}
	}
	if (y)
		try {
			!(function() {
				var e = new Date().getTime(),
					n = i(g)
				n && e - 1e3 * n.timestamp < 66e5 ? c(n) : s()
			})(),
				U.isWxapp(function(e) {
					e && f()
				})
		} catch (e) {}
	return U
})
