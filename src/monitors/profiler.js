import jdProfiler from '@jmfe/jd-profiler'

jdProfiler.init({
	flag: 10,
	autoReport: true,
	autoAddStaticReport:true,
	autoAddApiReport:true
})
